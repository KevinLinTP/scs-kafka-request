package com.thinkpower.springcloudstreamkafka.config;

import com.thinkpower.springcloudstreamkafka.channel.MyProcessor;
import com.thinkpower.springcloudstreamkafka.dto.MessageDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.HeaderEnricherSpec;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.Message;

import java.util.UUID;

@Configuration
public class CloudStreamConfig {
    private static final String ENRICH = "enrich";
    private static final String FILTER = "filter";
    private static final String INSTANCE_ID = "instanceId";
    private static final UUID INSTANCE_UUID = UUID.randomUUID();

    @MessagingGateway()
    public interface StreamGateway {
        @Gateway(requestChannel = ENRICH, replyChannel = MyProcessor.INPUT, requestTimeout = 3000)
        Message<byte[]> sendAndReceive(Message<MessageDTO> message);
    }

    @Bean
    public IntegrationFlow headerEnrichFlow() {
        return IntegrationFlows
                .from(ENRICH)
                .enrichHeaders(HeaderEnricherSpec::headerChannelsToString)
                .enrichHeaders(headerEnricherSpec -> headerEnricherSpec.header(INSTANCE_ID, INSTANCE_UUID))
                .channel(MyProcessor.OUTPUT)
                .get();
    }

    @Bean
    public IntegrationFlow replyFiltererFlow() {
        return IntegrationFlows
                .from(MyProcessor.INPUT)
                .filter(Message.class, message -> INSTANCE_UUID.equals(message.getHeaders().get(INSTANCE_ID)))
                .channel(FILTER)
                .get();
    }

}
