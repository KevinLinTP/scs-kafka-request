package com.thinkpower.springcloudstreamkafka.controller;

import com.thinkpower.springcloudstreamkafka.dto.MessageDTO;
import com.thinkpower.springcloudstreamkafka.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    MessageService service;

    // get the String message via HTTP, publish it to broker using spring cloud stream
    @PostMapping(value = "/sendMessage")
    public MessageDTO publishMessageString(@RequestBody MessageDTO payload) {

        Message<byte[]> replyMessage = service.sendAndReceive(payload);
        LOGGER.info("sendAndReceive : {}", replyMessage);

        return service.transReceive(replyMessage);
    }
}

