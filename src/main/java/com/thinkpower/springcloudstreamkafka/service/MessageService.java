package com.thinkpower.springcloudstreamkafka.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinkpower.springcloudstreamkafka.channel.MyProcessor;
import com.thinkpower.springcloudstreamkafka.config.CloudStreamConfig.StreamGateway;
import com.thinkpower.springcloudstreamkafka.dto.MessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Service
@EnableBinding(MyProcessor.class)
public class MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    private StreamGateway streamGateway;

    @Autowired
    private ObjectMapper objectMapper;


    public Message<byte[]> sendAndReceive(MessageDTO dto) {
        Message<MessageDTO> message = MessageBuilder
                .withPayload(dto)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

        return streamGateway.sendAndReceive(message);
    }

    public MessageDTO transReceive(Message<byte[]> replyMessage) {
        LOGGER.info("received Message : {}", replyMessage);
        MessageDTO dto;
        try {
            dto = objectMapper.readValue(replyMessage.getPayload(), MessageDTO.class);
        } catch (Exception e) {
            LOGGER.error("transReceive error : ", e);
            dto = new MessageDTO();
        }
        return dto;
    }

    @StreamListener("errorChannel")
    public void errorGlobal(ErrorMessage errorMessage) {
        LOGGER.error("In ErrorGlobal");
        LOGGER.error("Error : ", errorMessage.getPayload());
    }
}
